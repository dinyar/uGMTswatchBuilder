# Use CC7 as base
FROM cern/cc7-base:latest

# Get XDAQ 14
COPY repo_files/xdaq.repo /etc/yum.repos.d/xdaq.repo
RUN yum clean all
RUN yum -y groupinstall extern_coretools coretools extern_powerpack powerpack database_worksuite general_worksuite hardware_worksuite

# Get TS 5.1
COPY repo_files/cactus-ts.repo /etc/yum.repos.d/cactus-ts.repo
RUN yum clean all
RUN yum -y groupinstall triggersupervisor

# Get uHAL v2.6
COPY repo_files/ipbus-sw.repo /etc/yum.repos.d/ipbus-sw.repo
RUN yum clean all
RUN yum -y groupinstall uhal

# Get SWATCH
COPY repo_files/cactus-swatch.repo /etc/yum.repos.d/cactus-swatch.repo
RUN yum clean all
RUN yum -y groupinstall swatch swatchcell
RUN yum -y install boost-devel pugixml-devel

# Get AMC13 SWATCH plugin
COPY repo_files/cactus-amc13.repo /etc/yum.repos.d/cactus-amc13.repo
RUN yum clean all
RUN yum -y install cactusboards-amc13-amc13-1.2.8 cactusboards-amc13-tools-1.2.8 cactusboards-amc13-python-1.2.8
RUN yum -y install cactuscore-swatch-amc13

# Get MP7 SWATCH plugin
COPY repo_files/cactus-mp7.repo /etc/yum.repos.d/cactus-mp7.repo
RUN yum clean all
RUN yum -y groupinstall mp7
RUN yum -y install cactuscore-swatch-mp7

# Build uGMT cell code
RUN yum -y install make gcc-c++ rpm-build libuuid-devel
WORKDIR /ugmt_cell
COPY config config
COPY cactusprojects cactusprojects
COPY cactusupgrades cactusupgrades
WORKDIR cactusupgrades/projects/ugmt/software
RUN make
RUN make rpm
RUN yum -y localinstall ugmt/rpm/cactusboards-ugmt-0.3.1-0.cc7.x86_64.rpm
WORKDIR /ugmt_cell/cactusprojects/ugmt
RUN make
RUN make rpm
RUN yum -y localinstall hardware/rpm/cactusprojects-ugmthardware-0.6.4-1.0.centos7.gcc4_8_5.x86_64.rpm ts/cell/rpm/cactusprojects-ugmttscell-0.4.2-1.0.centos7.gcc4_8_5.x86_64.rpm

