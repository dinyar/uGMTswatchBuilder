#!/bin/bash

mkdir -p config
pushd config
svn co svn+ssh://dinyar@svn.cern.ch/reps/cactus/trunk/cactuscore/ts/config .
popd
mkdir -p cactusprojects/ugmt
pushd cactusprojects/ugmt
svn co svn+ssh://dinyar@svn.cern.ch/reps/cactus/trunk/cactusprojects/ugmt .
popd
mkdir -p cactusupgrades/projects/ugmt/software
pushd cactusupgrades/projects/ugmt/software
svn co svn+ssh://dinyar@svn.cern.ch/reps/cactus/trunk/cactusupgrades/projects/ugmt/software .
